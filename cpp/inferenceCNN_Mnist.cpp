#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <cuda_runtime_api.h>
#include <fstream>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <unordered_map>
#include <cassert>
#include <vector>
#include "NvUffParser.h"
#include "NvInfer.h"


#include "common.h"

using namespace nvuffparser;
using namespace nvinfer1;

static Logger gLogger;

ICudaEngine* loadModelAndCreateEngine( const char* uffFile, int maxBatchSize, IUffParser* parser)
{
	IBuilder* builder = createInferBuilder(gLogger);
	INetworkDefinition* network = builder->createNetwork();

	if (!parser->parse(uffFile, *network, nvinfer1::DataType::kFLOAT)) //type on which the weights will transformed in
	{
		 printf("Error : failed to parse\n");
	}
	
	builder->setMaxBatchSize(maxBatchSize);
	builder->setMaxWorkspaceSize(1 << 20);

	ICudaEngine* engine = builder->buildCudaEngine(*network);
	
	if(!engine)
	{
		printf("Error : failed to create engine\n");
	}
	
	network->destroy();
	builder->destroy();
	
	return engine;
}


int main( int argc, char ** argv)
{
	// load UffFile

	const char* filename = "../frozen_graph.uff";

	// Parse file

	int maxBatchSize = 1;
	IUffParser parser; // = createUffParser();

	parser->registerInput("Placeholder", Dims3(1,28,28), UffInputOrder::kNCHW); // 0 
	parser->registerOutput("Relu");

	// Create ICudaEngine

	ICudaEngine* engine = loadModelAndCreateEngine( filename, maxBatchSize, parser);

	parser->destroy();

	// Loading test image

	float img[28][28];

	// Prediction

	IExecutionContext* context = engine->createExecutionContext();

	int batchSize = 1;
	
	int nbBindings = engine->getNbBindings();

	std::vector<void *> bufferGPU(nbBindings);

	float output[10];

	int bufferSizeInput = 28*28;
	int bufferSizeOutput = 10*1;

	cudaMalloc(&bufferGPU[0], bufferSizeInput * sizeof(float));
	cudaMalloc(&bufferGPU[1], bufferSizeOutput * sizeof(float));

	cudaMemcpy(bufferGPU[0], img, bufferSizeInput*sizeof(float), cudaMemcpyHostToDevice);

	context->execute(batchSize, &bufferGPU[0]);

	cudaMemcpy(output, bufferGPU[1], bufferSizeOutput*sizeof(float), cudaMemcpyDeviceToHost);

	// Looking for Max proba

	float max = 0.0;
	int maxIndx = 0;
	for(int i = 0 ; i<10, i++)
	{
		if(output[i] > max)
		{
			max = output[i];
			maxIndx = i;
		}
	}
	
	printf("Prediction : %i | \n", max);

	cudaFree(bufferGPU[0]);
	cudaFree(bufferGPU[1]);

	context->destroy();
	engine->destroy();

	return 0;
	
}
