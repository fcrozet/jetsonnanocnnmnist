from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
import numpy as np
import time

# Pour otpimisation sur GPU et prédiction
import tensorrt as trt
from tensorrt.parsers import uffparser
import uff
import py.cuda as cuda
import pycuda.autoinit

Model_DIR = os.path.dirname(os.path.realpath("."))

# Chargement des données
mnist = input_data.read_data_sets("../data/MNIST_data", one_hot=True)
num_examples = mnist.train.images.shape[0] 
num_input = mnist.train.images.shape[1]
num_classes = mnist.train.labels.shape[1]

num_epochs = 1
batch_size = 100
total_batch = int(num_examples/batch_size)

K1 = 5
N1 = 32

K2 = 5
N2 = 64

N3 = 1024
N4 = 10

x_ = tf.placeholder(tf.float32 , shape=[None,num_input]) #28X28 
y_ = tf.placeholder(tf.float32 , shape=[None,num_classes])#0->9

kernel1 =  tf.Variable(tf.random_normal(shape=[K1, K1, 1, N1]))
b1 = tf.Variable(tf.zeros ([N1]))
conv1 = tf.nn.conv2d(tf.reshape(x_,[-1,28,28,1]), kernel1, [1, 1, 1, 1], padding='SAME')
y1 = tf.nn.relu(tf.nn.bias_add(conv1, b1))

yp1 = tf.nn.max_pool(y1, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

kernel2 =  tf.Variable(tf.random_normal(shape=[K2, K2, N1, N2]))
b2 = tf.Variable(tf.zeros ([N2]))
conv2 = tf.nn.conv2d(yp1, kernel2, [1, 1, 1, 1], padding='SAME')
y2 = tf.nn.relu(tf.nn.bias_add(conv2, b2))

yp2 = tf.nn.max_pool(y2, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

b3 = tf.Variable(tf.zeros ([N3]))
W3 = tf.Variable(tf.random_normal([7*7*N2,N3]))
y3 = tf.nn.relu(tf.matmul(tf.reshape(yp2,[-1,W3.get_shape().as_list()[0]]),W3)+b3)

b4 = tf.Variable(tf.zeros ([N4]))
W4 = tf.Variable(tf.random_normal([N3,N4]))
y = tf.matmul(y3,W4)+b4

cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=y))
train_step = tf.train.AdamOptimizer(0.01).minimize(cost)
correct_pred = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
init = tf.global_variables_initializer()
saver = tf.train.Saver()

with tf.Session() as sess:

	sess.run(init)
	start_training = time.time()

	saver.restore(sess, "./tmp/modelCNN_Mnist.ckpt")

	#Convertion graph en UFF
	output_name = ["Relu"]
	frozen_graph = tf.graph_util.convert_variables_to_constants(sess, sess.graph.as_graph_def(), output_name)
	frozen_graph = tf.graph_util.remove_training_nodes(frozen_graph) 
	
        model_path = os.path.join(Model_DIR, "frozen_graph.uff")

	uff_model = uff.from_tensorflow(frozen_graph, output_name, output_filename= model_path)
	
	LOGGER = trt.Logger(trt.Logger.ERROR)
	parser = trt.UffParser()
	parser.register_input("Placeholder", [1,28,28])
	parser.register_output("Relu")

	#Creation de l'engine
	network = trt.Builder(LOGGER).create_network()
	parser.parse(uff_model, network)
	engine = trt.Builder(LOGGER).build_cuda_engine(network)

	#engine = trt.utils.uff_to_trt_engine(LOGGER, uff_model, parser, 1 , 1<<20) #existe plus depuis la version 5

	del parser
	del network

	runtime = trt.Runtime(LOGGER)
        context = engine.create_execution_context()

        startC = cuda.Event()
        stopC = cuda.Event()

	#Image test
        image = mnist.test.images[:10]
        labelL = mnist.test.labels[:10]

        good_pred = 0

        for i in range(1) :
                img = image[i]
                img = img.astype(np.float32)
                label = labelL[i]
                print( "Inference " + str(i))

                output = np.empty(10, dtype=np.float32)
                #d_input = cuda.mem_alloc(1*img.size*img.dtype.itemsize)
                d_input = cuda.mem_alloc_like(img)
                #d_output = cuda.mem_alloc(1*output.size*output.dtype.itemsize)
                d_output = cuda.mem_alloc_like(output)

                #print(str(d_input) + " | " + str(d_output))
                bindings = [d_input, d_output]
                #print(bindings)
                stream = cuda.Stream()
                start = time.time()
                cuda.memcpy_htod_async(d_input, img, stream)
                startC.record()
                context.execute_async(1, bindings, stream.handle, None)
                stopC.record()
                cuda.memcpy_dtoh_async(output, d_output, stream)
                end = time.time()
                #stream.synchronize()

                print("Label : " + str(np.argmax(label)) +" |  Prediction : " + str(np.argmax(output)) + "  | temps : " + "{:.8}".format(end-start) ) #  + " | Sortie : " + str(output)
                print("Temps kernel : " + str(startC.time_till(stopC)))
                #print("Test : " , sess.run(accuracy, feed_dict={x_ : [img], y_ : [label]}))
                if ( np.argmax(label) == np.argmax(output) ) :
                        good_pred += 1

                print(output)
                output = np.zeros(10, dtype=np.float32)
                cuda.memcpy_htod(d_output, output)
                cuda.memcpy_dtoh(output, d_output)
                print(output)

                del d_input #free the memory allocated
                del d_output
                del img
                print()


        print("Good Prediction : " , good_pred, "/ 10")
        '''
        with open("sample.engine", "wb") as f:
                f.write(engine.serialize())
        '''
        del context
        del runtime
        del engine

	
