from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
import numpy as np
import time


# Chargement des données
mnist = input_data.read_data_sets("../data/MNIST_data", one_hot=True)
num_examples = mnist.train.images.shape[0] 
num_input = mnist.train.images.shape[1]
num_classes = mnist.train.labels.shape[1]

num_epochs = 1
batch_size = 100
total_batch = int(num_examples/batch_size)

K1 = 5
N1 = 32

K2 = 5
N2 = 64

N3 = 1024
N4 = 10

x_ = tf.placeholder(tf.float32 , shape=[None,num_input]) #28X28 
y_ = tf.placeholder(tf.float32 , shape=[None,num_classes])#0->9

kernel1 =  tf.Variable(tf.random_normal(shape=[K1, K1, 1, N1]))
b1 = tf.Variable(tf.zeros ([N1]))
conv1 = tf.nn.conv2d(tf.reshape(x_,[-1,28,28,1]), kernel1, [1, 1, 1, 1], padding='SAME')
y1 = tf.nn.relu(tf.nn.bias_add(conv1, b1))

yp1 = tf.nn.max_pool(y1, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

kernel2 =  tf.Variable(tf.random_normal(shape=[K2, K2, N1, N2]))
b2 = tf.Variable(tf.zeros ([N2]))
conv2 = tf.nn.conv2d(yp1, kernel2, [1, 1, 1, 1], padding='SAME')
y2 = tf.nn.relu(tf.nn.bias_add(conv2, b2))

yp2 = tf.nn.max_pool(y2, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

b3 = tf.Variable(tf.zeros ([N3]))
W3 = tf.Variable(tf.random_normal([7*7*N2,N3]))
y3 = tf.nn.relu(tf.matmul(tf.reshape(yp2,[-1,W3.get_shape().as_list()[0]]),W3)+b3)

b4 = tf.Variable(tf.zeros ([N4]))
W4 = tf.Variable(tf.random_normal([N3,N4]))
y = tf.matmul(y3,W4)+b4

cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=y))
train_step = tf.train.AdamOptimizer(0.01).minimize(cost)
correct_pred = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
init = tf.global_variables_initializer()
saver = tf.train.Saver()

with tf.Session() as sess:
	sess.run(init)
	start_training = time.time()

	total_batch = int(num_examples/batch_size)
	for epoch in range(num_epochs):
		for step in range(total_batch):
			start = time.time()
			perm = np.arange(num_examples)
			np.random.shuffle(perm)
			indices = perm[0:batch_size]
			batch_x = mnist.train.images[indices]
			batch_y = mnist.train.labels[indices]
			sess.run([train_step, cost], feed_dict={x_: batch_x, y_: batch_y})
			end = time.time()
			print("Epoch :", '%03d' %(epoch+1), "temps :", "{:.5f}".format(end-start) )
	end_training = time.time()
	print("Temps entrainement : ", "{:.5f}".format(end_training-start_training))
	
	save_path = saver.save(sess, "./tmp/modelCNN_Mnist.ckpt")
	print("Modele sauvegarde dans le repertoire : %s" % save_path)
	
	print("Test:", sess.run(accuracy, feed_dict={x_: mnist.test.images[:256],y_: mnist.test.labels[:256]}))


